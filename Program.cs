﻿using System;

namespace csharp_una
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu() {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Menu:");
            Console.WriteLine("================================================");
            Console.WriteLine("");
            Console.WriteLine("- Calcular Salario: 1");
            Console.WriteLine("- Somatoria: 2");
            Console.WriteLine("- Produtos Variaveis: 3");
            Console.WriteLine("- Calcular valor: 4");
            Console.WriteLine("- Calcular Nova Salario Com Reajuste: 5");
            Console.WriteLine("- Troca Valores: 6");
            Console.WriteLine("- Dobro: 8");
            Console.WriteLine("- Media Ponderada: 9");
            Console.WriteLine("- Comprimento de uma Circunferência: 12");
            Console.WriteLine("- Farenheit para graus celsius: 13");
            Console.WriteLine("");
            Console.WriteLine("================================================");
            Console.WriteLine("");
            Console.WriteLine("");

            switch(Console.ReadLine()) {
                case "1":
                    CalcularSalario();
                    break;
                case "2":
                    Somatoria();
                    break;
                case "3":
                    ProdutoVariaveis();
                    break;
                case "4":
                    CalcularValor();
                    break;
                case "5":
                    CalcularNovaSalarioComReajuste();
                    break;
                case "6":
                    TrocaValores();
                    break;
                case "8":
                    Dobro();
                    break;
                case "9":
                    MediaPonderada();
                    break;
                case "12":
                    ComprimentoCircunferencia();
                    break;
                case "13":
                    FarenheitToCelsius();
                    break;
            }

            return;
        }

        static void CalcularSalario()
        {
            double salario, gratificacao = 0.05, imposto = 0.07;

            Console.WriteLine("Calcular Salario");
            Console.WriteLine("Favor informa o salario:");
            salario = double.Parse(Console.ReadLine());

            double paga = salario*imposto;
            Console.WriteLine("Paga de imposto:" + paga);

            Console.WriteLine("");

            double meta = gratificacao*salario;
            Console.WriteLine("Receber:" + meta);

            Console.WriteLine("");
            Console.WriteLine("Salario a receber:" + (salario - paga + meta));

            Console.WriteLine("");
            Console.WriteLine("");

            Menu();
        }

        static void Somatoria() {
            Console.WriteLine("Somatoria");

            Console.WriteLine("Favor informa um número:");
            int a = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Favor informa um novo número:");
            int b = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Favor informa um outro número:");
            int c = Int32.Parse(Console.ReadLine());

            int soma = a+b+c;

            Console.WriteLine("Soma: " + soma);

            Menu();
        }

        static void ProdutoVariaveis() {
            Console.WriteLine("Produtos Variaveis");

            Console.WriteLine("Favor informa um número:");
            int a = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Favor informa um novo número:");
            int b = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Favor informa um outro número:");
            int c = Int32.Parse(Console.ReadLine());


            int soma = a + b;
            Console.WriteLine("");
            Console.WriteLine("Soma: " + soma);

            int produto = b*c;
            Console.WriteLine("");
            Console.WriteLine("Produto: " + produto);

            Console.WriteLine("");
            Console.WriteLine("");

            Menu();
        }

        static void CalcularValor() {
            Console.WriteLine("Calcular Valor");
            double preco, quantidade;

            Console.WriteLine("Insira o valor do produto:");
            preco = double.Parse(Console.ReadLine());
            Console.WriteLine("Favor informa um novo número:");
            quantidade = double.Parse(Console.ReadLine());

            Console.WriteLine("");
            Console.WriteLine("Total: " + preco*quantidade);

            Console.WriteLine("");
            Console.WriteLine("");

            Menu();
        }

        static void CalcularNovaSalarioComReajuste()
        {
            Console.WriteLine("Calcular Nova Salario Com Reajuste");
            Console.WriteLine("");

            Console.WriteLine(".... Em breve ....");

            Console.WriteLine("");
            Console.WriteLine("");
            Menu();
        }

        static void TrocaValores() {
            Console.WriteLine("Troca Valores");
            Console.WriteLine("");

            Console.WriteLine("Digite o primeiro valor:");
            string a = Console.ReadLine();
            Console.WriteLine("Digite o segundo valor:");
            string b = Console.ReadLine();

            string c = b;
            b = a;
            a = c;

            Console.WriteLine("");
            Console.WriteLine("Primeiro valor: " + a);
            Console.WriteLine("Segundo valor: " + b);

            Console.WriteLine("");
            Console.WriteLine("");
            Menu();
        }

        static void Dobro() {
            Console.WriteLine("Dobro");
            Console.WriteLine("");

            Console.WriteLine("Digite um numero:");
            double a = double.Parse(Console.ReadLine());

            Console.WriteLine("");
            Console.WriteLine("Dobro: " + a*2);

            Console.WriteLine("");
            Console.WriteLine("");
            Menu();
        }

        static void MediaPonderada() {
            Console.WriteLine("Media Ponderada");
            Console.WriteLine("");

            Console.WriteLine("Digite a primeira nota:");
            double a = double.Parse(Console.ReadLine());

            Console.WriteLine("Digite a primeira Segunda nota:");
            double b = double.Parse(Console.ReadLine());

            double media = ((a*2)+(b*3))/5.0;

            Console.WriteLine("");
            Console.WriteLine("Primeira nota: " + a);
            Console.WriteLine("Peso 2");
            Console.WriteLine("");
            Console.WriteLine("Segunda nota: " + b);
            Console.WriteLine("Peso 3");
            Console.WriteLine("");
            Console.WriteLine("Media ponderada: " + media);

            Console.WriteLine("");
            Console.WriteLine("");
            Menu();
        }

        static void ComprimentoCircunferencia() {
            Console.WriteLine("Comprimento de uma Circunferência");
            Console.WriteLine("");

            Console.WriteLine("Digite a raio da circunferência:");
            double raio = double.Parse(Console.ReadLine());

            double comprimento = 2*raio*3.14;

            Console.WriteLine("");
            Console.WriteLine("PI: 3,14");
            Console.WriteLine("Comprimento: " + comprimento);

            Console.WriteLine("");
            Console.WriteLine("");
            
            Menu();
        }

        static void FarenheitToCelsius() {
            Console.WriteLine("- Farenheit para graus celsius");
            Console.WriteLine("");

            Console.WriteLine("Digite a temperatura em Farenheit:");
            double farenheit = double.Parse(Console.ReadLine());

            double celsius = ((5*farenheit)-160)/9;

            Console.WriteLine("");
            Console.WriteLine("C°: " + celsius);

            Console.WriteLine("");
            Console.WriteLine("");
            
            Menu();
        }
    }
}
